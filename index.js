const {
	collectible,
	discord_token,
	discord_channel
} = require('./config.json');
const fetch = require("node-fetch");
const Discord = require('discord.js');

function getting() {
	if (collectible == "true") {
		fetch(`https://api.hearthstonejson.com/v1/39282/frFR/cards.collectible.json`)
		.then(response => response.json())
		.then(data => {
			public_data = data
		})
	} else {
		fetch(`https://api.hearthstonejson.com/v1/39282/frFR/cards.json`)
		.then(response => response.json())
		.then(data => {
			public_data = data
		})
	}
	setTimeout(choosing, 1400)
}

function choosing() {
	if (public_data !== "Placeholder") {
		var compter = -1
		for (obj in public_data) {
			compter += 1
		}
		nombre = Math.floor((Math.random() * compter) + 0)
		identifiant = public_data[nombre].id
		if (discord_ready == "true") {
			try {
				client.channels.get(discord_channel).send({files: [`https://art.hearthstonejson.com/v1/render/latest/frFR/512x/${identifiant}.png`]})
				.catch(error => {
					console.error(error)
					console.log("Traduction: Incapable de trouver la carte choisie. Une nouvelle sera choisie avant que le bot se déconnecte. Probablement.")
					getting()
				})
				setTimeout(discord_logoff, 12000)
			}
			catch {
				console.log("Mauvais salon Discord, changez discord_channel dans config.json")
				process.exit()
			}
		} else {
			console.log("Il semblerait que le bot n'ait pas réussit à se connecter à Discord, vérifiez discord_token dans config.json")
			console.log("Le bot va retenter de se connecter et prendre une nouvelle carte dans 10 secondes...")
			setTimeout(discord_login, 10000)
			setTimeout(choosing, 12000)
		}
	} else {
		console.log("Erreur, les données desirées n'ont pas pu être obtenues")
		console.log("Nouvel essai dans 5 secondes...")
		setTimeout(getting, 5000)
	}
}

function discord_login() {
	client = new Discord.Client()
	client.login(discord_token)
	.catch(console.error)
	client.once('ready', () => {
		discord_ready = "true"
		console.log("\nConnecté à Discord !\n")
	});
	client.on('error', error => {
		console.error('(Hearthstone RCG, Discord) The websocket connection encountered an error:', error);
	});
}

function discord_logoff() {
	process.exit()
}

discord_ready = "false"
public_data = "Placeholder"
getting()
discord_login()
